import React from "react";

import "./Books.css";

// const lista = {
//   Titulo: "",
//   Autor: "",
//   year: "",
//   Categoria: "",
//   Saga: "",
//   image: "",
// };

class Books extends React.Component {
  // state = { lista: [] };

  getbooks() {
    fetch("http://localhost:3004/books")
      .then((response) => {
        return response.json();
      })
      .then((Books) => {
        this.setState({ booksList: Books });
        console.log(Books);
      });
  }

  render() {
    const lista = this.props.lista;

    return (
      <section>
        {lista && (
          <div>
            <h1>{lista.Titulo}</h1>
            <p>{lista.Autor} </p>
            <p>{lista.year}</p>
            <p>{lista.Categoria}</p>
            <p>{lista.Saga}</p>
            {/* <img src="{lista.image}" alt="" /> */}
          </div>
        )}
      </section>
    );
  }
}

export default Books;
// render() {
//   const { Titulo, Autor, year, Categoria, Saga, image } = this.props.lista;
//   return (
//     <section>
//       <h1>Mi lista de libros</h1>
//       {/* <h2> Id: {id} </h2> */}
//       <p>Titulo: {Titulo}</p>

//       <p>Autor:{Autor} </p>
//       <p>año:{year}</p>
//       <p>Categoria:{Categoria} </p>
//       <p>Saga:{Saga} </p>
//       <p>imagen{image} </p>
//     </section>
//   );
// }
// }
