import React from "react";
import "./Navbar.css";

const navbarLinks = ["Home", "About", "My List", "Register"];

class Navbar extends React.Component {
  render() {
    return (
      <nav className="navbar">
        <section>
          <ul className=" nav">
            {navbarLinks.map((linkName) => {
              return <li key={linkName}>{linkName}</li>;
            })}
          </ul>
        </section>
      </nav>
    );
  }
}

export default Navbar;
