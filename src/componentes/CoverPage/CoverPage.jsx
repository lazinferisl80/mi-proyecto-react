import React from "react";
import "./CoverPage.css";
import { Link } from "react-router-dom";

const logo = {
  name: "Entre Lineas",
  image: "../../librodefuego.jpg",
};

class CoverPage extends React.Component {
  render() {
    return (
      <section>
        <div className="tituloPagina">
          <h1>{logo.name}</h1>
        </div>

        <nav className="navbar">
          <div className="nav">
            <h1>
              {" "}
              <cite>
                {" "}
                Aprender a leer es encender un fuego, cada silaba que se
                deletrea es una chispa.
              </cite>
              <p>Victor Hugo</p>
            </h1>
            <div>
              <li>
                <Link to="/login">Inicia Sesion</Link>
              </li>
            </div>
            <li>
              <Link to="/home">Home</Link>
            </li>

            <div>
              <li>
                <Link to="/register">Registrate</Link>
              </li>
            </div>

            <div>
              <li>
                <Link to="/books">Libros</Link>
              </li>
            </div>
          </div>
        </nav>
        <img src="../../librodefuego.jpg" alt="" />
      </section>
    );
  }
}

export default CoverPage;
