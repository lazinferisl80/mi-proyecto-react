import "./App.css";
import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
//import Navbar from "./componentes/Navbar";
import CoverPage from "./componentes/CoverPage";
import Home from "./componentes/Home";
import Books from "./componentes/Books";
import RegisterForm from "./containers/RegisterForm";
import LoginForm from "./containers/LoginForm";
import { register } from "./api/auth";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { booksList: [] };

    console.log("¡Construyendo el componente!");
  }
  submitRegister = (form) => {
    register(form);
  };
  UNSAFE_componentWillMount() {
    console.log("Vamos alla");
  }

  componentDidMount() {
    console.log("ya estamos");
    fetch("http://localhost:3004/books")
      .then((response) => {
        return response.json();
      })
      .then((Books) => {
        this.setState({ booksList: Books });
        console.log(Books);
      });
    // this.getbooks();
  }

  render() {
    return (
      <div className="App">
        <Router>
          <header>
            <Switch>
              <Route path="/register" exact component={RegisterForm} />
              <Route path="/login" exact component={LoginForm} />
              <Route path="/Books" exact component={Books} />
              <Route path="/home" exact component={Home} />
              <Route path="/" component={CoverPage} />
            </Switch>
          </header>
          <div>
            {this.state.booksList.length ? (
              this.state.booksList.map((list) => {
                return (
                  <Books
                    lista={list}
                    key={`${list.Titulo}-${list.Autor}-${list.year}-${list.Categoria}-${list.Saga}-${list.image}`}
                  />
                );
              })
            ) : (
              <p>no hay lista</p>
            )}
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
// {this.state.booksList.length ? (
//             this.state.booksList.map((lista) => <Books lista={lista} />)()
//           ) : (
