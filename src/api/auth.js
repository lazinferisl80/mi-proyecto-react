const registerURL = "https://upgrade-auth.herokuapp.com/api/auth/register";
const loginURL = "https://upgrade-auth.herokuapp.com/api/auth/login";
const BooksURL = "http://localhost:3004/books";
const DetallesURL = "http://localhost:3004/detalles";

export const register = async (userData) => {
  const request = await fetch(registerURL, {
    method: "POST",
    headers: {
      Acept: "aplication/json",
      "content-type": "application/json",
      "access-Control-Allow-origin": "http://localhost:*",
      "Acces-Control-Allow-Credentials": "*",
    },
    credentials: "include",
    body: JSON.stringify(userData),
  });

  const response = await request.json();

  if (!request.ok) {
    throw new Error(response.message);
  }

  return response.data;
};

export const login = async (userData) => {
  const request = await fetch(loginURL, {
    method: "POST",
    headers: {
      Acept: "aplication/json",
      "content-type": "application/json",

      "access-Control-Allow-origin": "*",
    },
    credentials: "include",
    body: JSON.stringify(userData),
  });

  const response = await request.json();

  if (!request.ok) {
    throw new Error(response.message);
  }

  return response.data;
};
export const books = async (userData) => {
  const request = await fetch(BooksURL, {
    method: "POST",
    headers: {
      Acept: "aplication/json",
      "content-type": "application/json",
      //  "access-Control-Allow-origin": "http://localhost:*",
      "access-Control-Allow-origin": "*",
    },
    credentials: "include",
    body: JSON.stringify(userData),
  });

  const response = await request.json();

  if (!request.ok) {
    throw new Error(response.message);
  }

  return response.data;
};
export const detalles = async (userData) => {
  const request = await fetch(DetallesURL, {
    method: "POST",
    headers: {
      Acept: "aplication/json",
      "content-type": "application/json",
      //"access-Control-Allow-origin": "http://localhost:*",
      "access-Control-Allow-origin": "*",
    },
    credentials: "include",
    body: JSON.stringify(userData),
  });

  const response = await request.json();

  if (!request.ok) {
    throw new Error(response.message);
  }

  return response.data;
};
