import React from "react";
import "./LoginForm.css";
import { login } from "../../api/auth";

const FormInitial = {
  username: "",
  password: "",
  error: "",
};

class LoginForm extends React.Component {
  state = FormInitial;

  handleSubmitForm = async (ev) => {
    ev.preventDefault();

    try {
      const data = await login(this.state);
      console.log("Login Completado", data);
      this.setState(FormInitial);
    } catch (error) {
      //this.setState({ error: error.message });
    }
  };

  handleChangeInput = (ev) => {
    const { name, value } = ev.target;
    this.setState({ [name]: value });
  };

  render() {
    return (
      <div className="login">
        <form onSubmit={this.handleSubmitForm}>
          <h1>Login</h1>
          {/* <label htmlFor="username">
          <p>Nombre de usuario</p>
          <input
            type="text"
            name="username"
            value={this.state.username}
            onChange={this.handleChangeInput}
          />
        </label> */}
          <label htmlFor="email">
            <p>Email</p>
            <input
              type="text"
              name="email"
              value={this.state.email}
              onChange={this.handleChangeInput}
            />
          </label>
          <label htmlFor="password">
            <p>password</p>
            <input
              type="password"
              name="password"
              value={this.state.password}
              onChange={this.handleChangeInput}
            />
          </label>

          {this.state.error && (
            <p style={{ color: "red" }}>
              Ha ocurrido un error con el Login:{this.state.error}
            </p>
          )}
          {this.state && <p>bienvenido {this.username}</p>}
          <button type="submit">Login</button>
        </form>
      </div>
    );
  }
}

export default LoginForm;
