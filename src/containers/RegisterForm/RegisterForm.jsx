import React from "react";
import { register } from "../../api/auth";

const FormInitial = {
  username: "",
  email: "",
  password: "",
  confirmPassword: "",
};

class RegisterForm extends React.Component {
  state = FormInitial;

  handleSubmitForm = async (ev) => {
    ev.preventDefault();

    try {
      const data = await register(this.state);
      console.log("Registro Completado", data);
      this.setState(FormInitial);
    } catch (error) {
      this.setState({ error: error.message });
    }
  };

  handleChangeInput = (ev) => {
    const { name, value } = ev.target;
    this.setState({ [name]: value });
  };

  render() {
    return (
      <form onSubmit={this.handleSubmitForm}>
        <h1>Registro</h1>
        <label htmlFor="username">
          <p>Nombre de usuario</p>
          <input
            type="text"
            name="username"
            value={this.state.username}
            onChange={this.handleChangeInput}
          />
        </label>
        <label htmlFor="email">
          <p>Email</p>
          <input
            type="text"
            name="email"
            value={this.state.email}
            onChange={this.handleChangeInput}
          />
        </label>
        <label htmlFor="password">
          <p>password</p>
          <input
            type="password"
            name="password"
            value={this.state.password}
            onChange={this.handleChangeInput}
          />
        </label>

        {this.state.error && (
          <p style={{ color: "red" }}>
            Ha ocurrido un error con el registro:{this.state.error}
          </p>
        )}
        <button type="submit">Enviar</button>
      </form>
    );
  }
}

export default RegisterForm;
